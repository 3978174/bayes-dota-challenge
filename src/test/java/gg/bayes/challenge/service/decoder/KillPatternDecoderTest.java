package gg.bayes.challenge.service.decoder;

import gg.bayes.challenge.data.Kill;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class KillPatternDecoderTest {
  private final KillDecoder decoder = new KillDecoder();
  private static final List<String> matchingLines = List.of(
      "[00:19:28.403] npc_dota_gyrocopter_homing_missile is killed by npc_dota_gyrocopter_homing_missile",
      "[00:19:29.736] npc_dota_hero_grimstroke is killed by npc_dota_hero_gyrocopter",
      "[00:19:30.335] npc_dota_lycan_wolf2 is killed by npc_dota_hero_centaur",
      "[00:19:31.069] npc_dota_neutral_forest_troll_berserker is killed by npc_dota_hero_keeper_of_the_light"
  );
  private static final List<String> notMatchingLines = List.of(
      "[00:19:29.069] npc_dota_hero_gyrocopter hits npc_dota_hero_grimstroke with gyrocopter_rocket_barrage for 9 damage (88->79)",
      "[00:19:38.800] npc_dota_hero_lycan uses item_quelling_blade",
      "[00:19:39.600] npc_dota_hero_monkey_king buys item item_clarity"
  );

  @Test
  public void testPattern() {
    Pattern pattern = decoder.getPattern();

    for (String matchingLine : matchingLines) {
      Matcher matcher = pattern.matcher(matchingLine);
      Assertions.assertTrue(matcher.matches());
    }

    for (String notMatchingLine : notMatchingLines) {
      Matcher matcher = pattern.matcher(notMatchingLine);
      Assertions.assertFalse(matcher.matches());

    }
  }

  @Test
  public void testDecode() {
    Optional<Kill> decode = decoder.decode(matchingLines.get(0));
    Kill kill = decode.get();
    Assertions.assertEquals("npc_dota_gyrocopter_homing_missile", kill.getVictim());
    Assertions.assertEquals("npc_dota_gyrocopter_homing_missile", kill.getKiller());
  }

}
