package gg.bayes.challenge.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Damage implements MatchEvent{
  @Id
  @GeneratedValue
  private Long id;
  private String author;
  private String source;
  private String target;
  private Integer damage;
  private Long timestamp;
  private Integer hpBefore;
  private Integer hpAfter;
  @ManyToOne
  private Match match;

}
