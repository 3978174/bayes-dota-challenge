package gg.bayes.challenge.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Kill implements MatchEvent {
  @Id
  @GeneratedValue
  private Long id;
  private String killer;
  private String victim;
  private Long timestamp;
  @ManyToOne
  private Match match;

}
