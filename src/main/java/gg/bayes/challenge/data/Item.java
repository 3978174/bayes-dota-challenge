package gg.bayes.challenge.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Item implements MatchEvent {
  @Id
  @GeneratedValue
  private Long id;
  private String hero;
  private String item;
  private Long timestamp;
  @ManyToOne
  private Match match;

}
