package gg.bayes.challenge.data;

public interface MatchEvent {

  void setMatch(Match match);

  Match getMatch();
}
