package gg.bayes.challenge.data;

import java.time.LocalDateTime;
import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Match {
  @Id
  @GeneratedValue
  private Long id;
  private Long dateCreated;
  @OneToMany(mappedBy = "match")
  private Collection<Damage> damages;
  @OneToMany(mappedBy = "match")
  private Collection<Item> items;
  @OneToMany(mappedBy = "match")
  private Collection<Kill> kills;
  @OneToMany(mappedBy = "match")
  private Collection<Spell> spells;

}
