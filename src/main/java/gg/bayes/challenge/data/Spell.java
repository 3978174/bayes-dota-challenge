package gg.bayes.challenge.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Spell implements MatchEvent {
  @Id
  @GeneratedValue
  private Long id;
  private String hero;
  private String spell;
  private Integer spellLevel;
  private String target;
  private Long timestamp;
  @ManyToOne
  private Match match;

}
