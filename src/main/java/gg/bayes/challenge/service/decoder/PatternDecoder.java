package gg.bayes.challenge.service.decoder;

import java.util.regex.Pattern;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class PatternDecoder<T> implements Decoder<T>{
  @Getter
  protected final Pattern pattern;

  public boolean supports(String line) {
    return pattern.matcher(line).matches();
  }

  protected static long getTimestamp(String hours, String minutes, String seconds, String milli) {
    return Long.parseLong(milli) +
           Long.parseLong(seconds) * 1000 +
           Long.parseLong(minutes) * 60 * 1000 +
           Long.parseLong(hours) * 60 * 60 * 1000;
  }
}
