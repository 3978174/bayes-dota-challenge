package gg.bayes.challenge.service.decoder;

import gg.bayes.challenge.data.Kill;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

@Component
public class KillDecoder extends PatternDecoder<Kill> {

  public KillDecoder() {
    super(Pattern.compile("\\[(\\d{2}):(\\d{2}):(\\d{2}).(\\d{3})] npc_dota_hero_(\\w+) is killed by npc_dota_hero_(\\w+)"));
  }

  @Override
  public Optional<Kill> decode(String line) {
    Matcher matcher = pattern.matcher(line);
    if (!matcher.matches()) {
      throw new IllegalArgumentException();
    }
    Kill kill = new Kill();
    long ts = getTimestamp(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4));
    kill.setTimestamp(ts);
    kill.setVictim(matcher.group(5));
    kill.setKiller(matcher.group(6));
    return Optional.of(kill);
  }

}
