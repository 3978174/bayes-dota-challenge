package gg.bayes.challenge.service.decoder;

import java.util.Optional;

public interface Decoder<T> {

  Optional<T> decode(String line);
}
