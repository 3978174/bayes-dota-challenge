package gg.bayes.challenge.service.decoder;

import gg.bayes.challenge.data.Item;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

@Component
public class ItemDecoder extends PatternDecoder<Item> {

  public ItemDecoder() {
    super(Pattern.compile("\\[(\\d{2}):(\\d{2}):(\\d{2}).(\\d{3})] npc_dota_hero_(\\w+) buys item item_(\\w+)"));
  }

  @Override
  public Optional<Item> decode(String line) {
    Matcher matcher = pattern.matcher(line);
    if (!matcher.matches()) {
      throw new IllegalArgumentException();
    }
    Item item = new Item();
    long ts = getTimestamp(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4));
    item.setTimestamp(ts);
    item.setHero(matcher.group(5));
    item.setItem(matcher.group(6));
    return Optional.of(item);
  }

}
