package gg.bayes.challenge.service.decoder;

import gg.bayes.challenge.data.MatchEvent;
import java.util.Optional;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DecoderProvider implements Decoder<MatchEvent>{
  private final Set<PatternDecoder<? extends MatchEvent>> decoders;

  @Override
  public Optional<MatchEvent> decode(String line) {
    for (PatternDecoder<? extends MatchEvent> decoder : decoders) {
      if (decoder.supports(line)) {
        return (Optional<MatchEvent>) decoder.decode(line);
      }
    }
    return Optional.empty();
  }
}
