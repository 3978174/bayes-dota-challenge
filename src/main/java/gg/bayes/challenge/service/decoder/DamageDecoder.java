package gg.bayes.challenge.service.decoder;

import gg.bayes.challenge.data.Damage;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

@Component
public class DamageDecoder extends PatternDecoder<Damage> {

  public DamageDecoder() {
    super(Pattern.compile("\\[(\\d{2}):(\\d{2}):(\\d{2}).(\\d{3})] npc_dota_hero_(\\w+) hits npc_dota_hero_(\\w+) with (\\w+) for (\\d+) damage(?: \\((\\d+)->(\\d+)\\))?"));
  }

  @Override
  public Optional<Damage> decode(String line) {
    Matcher matcher = pattern.matcher(line);
    if (!matcher.matches()) {
      throw new IllegalArgumentException();
    }
    Damage damage = new Damage();
    long ts = getTimestamp(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4));
    damage.setTimestamp(ts);
    damage.setAuthor(matcher.group(5));
    damage.setTarget(matcher.group(6));
    damage.setSource(matcher.group(7));
    damage.setDamage(Integer.parseInt(matcher.group(8)));
    if (matcher.group(9) != null && matcher.group(10) != null) {
      damage.setHpBefore(Integer.parseInt(matcher.group(9)));
      damage.setHpAfter(Integer.parseInt(matcher.group(10)));
    }
    return Optional.of(damage);
  }

}
