package gg.bayes.challenge.service.decoder;

import gg.bayes.challenge.data.Spell;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

@Component
public class SpellDecoder extends PatternDecoder<Spell> {

  public SpellDecoder() {
    super(Pattern.compile("\\[(\\d{2}):(\\d{2}):(\\d{2}).(\\d{3})] npc_dota_hero_(\\w+) casts ability (\\w+) \\(lvl (\\d+)\\) on (\\w+)"));
  }

  @Override
  public Optional<Spell> decode(String line) {
    Matcher matcher = pattern.matcher(line);
    if (!matcher.matches()) {
      throw new IllegalArgumentException();
    }
    Spell spell = new Spell();
    long ts = getTimestamp(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4));
    spell.setTimestamp(ts);
    spell.setHero(matcher.group(5));
    spell.setSpell(matcher.group(6));
    spell.setSpellLevel(Integer.parseInt(matcher.group(7)));
    spell.setTarget(matcher.group(8));
    return Optional.of(spell);
  }
}
