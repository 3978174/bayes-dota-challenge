package gg.bayes.challenge.service.match.impl;

import gg.bayes.challenge.data.Match;
import gg.bayes.challenge.data.MatchEvent;
import gg.bayes.challenge.repo.MatchRepository;
import gg.bayes.challenge.rest.model.HeroDamage;
import gg.bayes.challenge.rest.model.HeroItems;
import gg.bayes.challenge.rest.model.HeroKills;
import gg.bayes.challenge.rest.model.HeroSpells;
import gg.bayes.challenge.service.decoder.DecoderProvider;
import gg.bayes.challenge.service.match.MatchService;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class MatchServiceImpl implements MatchService {
    private final DecoderProvider decoderProvider;
    private final MatchRepository matchRepository;
    private final EntityManager entityManager;

    public MatchServiceImpl(DecoderProvider decoderProvider,
                            MatchRepository matchRepository,
                            EntityManager entityManager) {
        this.decoderProvider = decoderProvider;
        this.matchRepository = matchRepository;
        this.entityManager = entityManager;
    }


    @Override
    @Transactional
    public Long ingestMatch(String payload) {
        Match match = new Match();
        match.setDateCreated(System.currentTimeMillis());
        matchRepository.saveAndFlush(match);
        String[] lines = payload.split("\n");
        for (String line : lines) {
            Optional<MatchEvent> eventOpt = decoderProvider.decode(line);
            if (eventOpt.isPresent()) {
                MatchEvent event = eventOpt.get();
                event.setMatch(match);
                entityManager.persist(event);
            }
        }
        return match.getId();
    }

    @Override
    public List<HeroKills> getHeroKills(Long matchId) {
        return matchRepository.getHeroKills(matchId);
    }

    @Override
    public List<HeroItems> getHeroItems(Long matchId, String heroName) {
        return matchRepository.getHeroItems(matchId, heroName);
    }

    @Override
    public List<HeroSpells> getHeroSpells(Long matchId, String heroName) {
        return matchRepository.getHeroSpells(matchId, heroName);
    }

    @Override
    public List<HeroDamage> getHeroDamage(Long matchId, String heroName) {
        return matchRepository.getHeroDamage(matchId, heroName);
    }

}
