package gg.bayes.challenge.repo;

import gg.bayes.challenge.data.Match;
import gg.bayes.challenge.rest.model.HeroDamage;
import gg.bayes.challenge.rest.model.HeroItems;
import gg.bayes.challenge.rest.model.HeroKills;
import gg.bayes.challenge.rest.model.HeroSpells;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MatchRepository extends JpaRepository<Match, String> {

  @Query("select new gg.bayes.challenge.rest.model.HeroKills(killer, CAST(count(id) as int)) " +
         "from Kill where match.id = :matchId group by killer")
  List<HeroKills> getHeroKills(@Param("matchId") Long matchId);

  @Query("select new gg.bayes.challenge.rest.model.HeroItems(item, timestamp) from Item " +
         "where match.id = :matchId and hero = :hero")
  List<HeroItems> getHeroItems(
      @Param("matchId") Long matchId,
      @Param("hero") String hero);

  @Query("select new gg.bayes.challenge.rest.model.HeroSpells(spell, CAST(count(id) as int)) from Spell " +
         "where match.id = :matchId and hero = :hero group by spell")
  List<HeroSpells> getHeroSpells(
      @Param("matchId") Long matchId,
      @Param("hero") String hero);

  @Query("select new gg.bayes.challenge.rest.model.HeroDamage(target, CAST(count(id) as int), CAST(sum(damage) as int)) from Damage " +
         "where match.id = :matchId and author = :hero group by target")
  List<HeroDamage> getHeroDamage(
      @Param("matchId") Long matchId,
      @Param("hero") String hero);
}
